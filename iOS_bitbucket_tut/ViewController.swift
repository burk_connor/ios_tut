//
//  ViewController.swift
//  iOS_bitbucket_tut
//
//  Created by Connor Burk on 7/17/17.
//  Copyright © 2017 Connor Burk. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var state_picker: UIPickerView!
    
    @IBOutlet weak var state_picker_button: UIButton!
    
    @IBOutlet weak var buy_now_button: UIButton!
    
    let states = ["Alaska", "Arkansas", "Alabama", "California"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        state_picker.dataSource = self
        state_picker.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func state_button_pressed(_ sender: Any) {
        
        state_picker.isHidden = false
        
    }
    
    @IBAction func buy_now_button_pressed(_ sender: Any) {
        
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        state_picker_button.setTitle(states[row], for: UIControlState.normal)
        state_picker.isHidden = false
    }

}

